object BracketPush {
  private val brackets = "(){}[]".toSet

  def isPaired(s: String): Boolean = {
    def isPairedHelper(brackets: List[Char], stack: List[Char]): Boolean = (brackets, stack) match {
      case (('(' | '{' | '[') :: xs, _) => isPairedHelper(xs, brackets.head +: stack)
      case (')' :: xs, '(' :: ys) => isPairedHelper(xs, ys)
      case ('}' :: xs, '{' :: ys) => isPairedHelper(xs, ys)
      case (']' :: xs, '[' :: ys) => isPairedHelper(xs, ys)
      case (Nil, Nil) => true
      case _ => false
    }
    isPairedHelper(s.filter(brackets).toList, List[Char]())
  }
}