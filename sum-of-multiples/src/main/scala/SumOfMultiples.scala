object SumOfMultiples {
  def sum(factors: Set[Int], limit: Int): Int = {
    (1 until limit).filter(n => factors.exists(f => n % f == 0)).sum
  }
}