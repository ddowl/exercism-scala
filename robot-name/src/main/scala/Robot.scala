import scala.collection.mutable
import scala.util.Random

object RobotFactory {
  private var numRobotsDeployed = 0

  // Assumes we never go past the end lol
  // Generates all names randomly with no repeats
  private lazy val randomNamePool = Random
    .shuffle(0 to 675999)
    .map(idToRobotName(_))

  private val redactedQueue = mutable.Queue[String]()

  def generateNewName(currName: Option[String] = None): String = {
    if (redactedQueue.isEmpty) {
      nextName
    } else {
      currName match {
        case None => redactedQueue.dequeue()
        case Some(name) =>
          if (redactedQueue.front.equals(name)) {
            nextName
          } else {
            redactedQueue.dequeue()
          }
      }
    }
  }

  def redactName(name: String) = {
    redactedQueue += name
  }

  // Establish bijunction between integers and robot names
  private def idToRobotName(id: Int): String = {
    val digits = id % 1000
    val secondLetter = (((id / 1000) % 26) + 'A').toChar.toString
    val firstLetter = (((id / 1000 / 26) % 26) + 'A').toChar.toString

    firstLetter + secondLetter + "%03d".format(digits)
  }

  // Robot counter must always be incremented when new name is picked
  // TODO: encapsulate in lazy Stream?
  private def nextName: String = {
    val newName = randomNamePool(numRobotsDeployed)
    numRobotsDeployed += 1
    newName
  }
}

class Robot {

  import RobotFactory._

  // During construction, assign a new unique name to Robot
  private var thisName = generateNewName()

  def name = thisName

  def reset() = {
    redactName(thisName)
    thisName = generateNewName(Some(thisName))
  }
}